package rw

import "testing"

func TestREADMEString(t *testing.T) {
	readme := loadREADME("testdata/README/rob-pike")
	t.Log(readme)
}

func TestTrimB(t *testing.T) {
	s1 := "something {here}"
	s2 := "something{here}"
	s3 := "something   {here}"
	s4 := "something   here}"
	t.Logf(`"%v"`, trimB(s1))
	t.Logf(`"%v"`, trimB(s2))
	t.Logf(`"%v"`, trimB(s3))
	t.Logf(`"%v"`, trimB(s4))
}
