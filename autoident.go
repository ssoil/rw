package rw

import uc "unicode"

// AutoIdent follows the Pandoc Markdown conventions for converting a natural
// language text string into a slug (auto identifier) suitable for using in
// web addresses and creating automatic identifiers for headings that are
// universally compatible for use in all document formats supported by Pandoc
// that allow automatic links to headings.
//
// At first glance this conversion might seem more annoying than, say, GitHub
// Flavored Markdown, but its compatibility is worth it. GFM does not consider
// anything but mostly English slug creation for HTML links.
//
// Applies the following Pandoc Markdown rules:
//
//     1. Remove everything up to the first letter (identifiers may not begin
//       with a number or punctuation mark).
//
//     2. Replace all spaces with hyphens.
//
//     3. Remove all non-alphanumeric characters, except underscores, hyphens,
//       and periods.
//
//     4. Convert all alphabetic characters to lowercase.
//
// There are a few slight differences from how Pandoc does this conversion (see
// Pandoc auto_identifiers and ParseHeadings):
//
//     * Return empty instead of 'section' when empty.
//
//     * Expect no newlines.
//
//     * Expect simplified inline formatting (*,**,***,`,~~)
//
//     * Expect no Pandoc Markdown footnotes.
//
// Go's excellent UNICODE-aware conversions are used throughout ensuring all
// languages are fully supported.
//
// As a matter of convenience if a Pandoc identifier is detected {#id} it will
// be returned instead without any format validation whatsoever. A curly bracket
// must be the last thing on the line for this to work.
// the last thing on the line. This allows content creators to override the
// default Pandoc auto-identifier algorithm
// completely. This is particularly important when working with duplicates
// of a given heading in the same document (which should always be avoided
// in any KNode in the first place).
func AutoIdent(hd string) string {
	ident := []rune{}
	rtext := []rune(hd)
	if rtext[len(rtext)-1] == '}' {
		n := len(rtext) - 1
		s := 0
		e := n
		for ; rtext[n] != '{' && n > 0; n-- {
			switch {
			case uc.IsSpace(rtext[n]):
				e = n
			case rtext[n] == '#':
				s = n + 1
				return string(rtext[s:e])
			}
		}
	}
	for _, r := range hd {
		switch {
		case len(ident) == 0 && !uc.IsLetter(r):
			continue
		case uc.IsSpace(r):
			ident = append(ident, '-')
		case uc.IsLetter(r):
			ident = append(ident, uc.ToLower(r))
		case uc.IsNumber(r) || r == '-' || r == '_' || r == '.':
			ident = append(ident, r)
		}
	}
	return string(ident)
}
