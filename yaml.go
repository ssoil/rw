package rw

import (
	"bufio"
	"github.com/ghodss/yaml"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

// ParseYAML parses YAML from bytes and returns a data object.
func ParseYAML(byt []byte) map[string]interface{} {
	yml := map[string]interface{}{}
	s := bufio.NewScanner(strings.NewReader(string(byt)))
	s.Scan()
	line := s.Text()
	buf := ""
	if !(len(line) == 3 && line[0:3] == "---") {
		return yml
	}
	buf += "---\n"
	for s.Scan() {
		line := s.Text()
		if len(line) == 3 && line == "---" {
			break
		}
		buf += line + "\n"
	}
	yaml.Unmarshal([]byte(buf), &yml)
	return yml
}

// ReadYAML reads and parses YAML front matter and returns. Note YAML
// can only be placed at the very beginning of a content node and
// must begin on the first line and must end with --- (not ...).
func ReadYAML(r io.Reader) map[string]interface{} {
	byt, _ := ioutil.ReadAll(r)
	return ParseYAML(byt)
}

// LoadYAMLLoad reads the YAML front matter from the specified file at path.
func LoadYAML(path string) map[string]interface{} {
	file, _ := os.Open(path)
	defer file.Close()
	return ReadYAML(file)
}
