package rw_test

import (
	"fmt"

	"gitlab.com/ssoil/rw"
)

func ExampleAutoIdent() {

	// These are the examples from the Pandoc Markdown documentation:

	fmt.Println(rw.AutoIdent("Heading identifiers in HTML"))
	fmt.Println(rw.AutoIdent("Maître d'hôtel"))
	fmt.Println(rw.AutoIdent("Dogs*?--in *my* house?"))
	fmt.Println(rw.AutoIdent("Dogs*?--in *my* house? {#dogs}"))
	fmt.Println(rw.AutoIdent("Dogs*?--in *my* house? {.some #dogs}"))
	fmt.Println(rw.AutoIdent("Dogs*?--in *my* house? {.some #dogs #and}"))
	fmt.Println(rw.AutoIdent("Dogs*?--in *my* house? {#dogs .some}"))
	fmt.Println(rw.AutoIdent("[HTML], [S5], or [RTF]?"))
	fmt.Println(rw.AutoIdent("3. Applications"))
	fmt.Println(rw.AutoIdent("33"))
	fmt.Println(rw.AutoIdent("KN^3.v3"))

	// Output:
	//
	// heading-identifiers-in-html
	// maître-dhôtel
	// dogs--in-my-house
	// dogs
	// dogs
	// and
	// dogs
	// html-s5-or-rtf
	// applications
	//
	// kn3.v3
}

func ExampleAutoIdent_two() {

	fmt.Println(rw.AutoIdent("# Heading identifiers in HTML"))
	fmt.Println(rw.AutoIdent("## Maître d'hôtel"))
	fmt.Println(rw.AutoIdent("### Dogs*?--in *my* house?"))
	fmt.Println(rw.AutoIdent("#### Dogs*?--in *my* house? {#dogs}"))
	fmt.Println(rw.AutoIdent("##### Dogs*?--in *my* house? {.some #dogs}"))
	fmt.Println(rw.AutoIdent("###### Dogs*?--in *my* house? {.some #dogs #and}"))
	fmt.Println(rw.AutoIdent("# Dogs*?--in *my* house? {#dogs .some}"))
	fmt.Println(rw.AutoIdent("# [HTML], [S5], or [RTF]?"))
	fmt.Println(rw.AutoIdent("# 3. Applications"))
	fmt.Println(rw.AutoIdent("# 33"))
	fmt.Println(rw.AutoIdent("# KN^3.v3"))

	// Output:
	//
	// heading-identifiers-in-html
	// maître-dhôtel
	// dogs--in-my-house
	// dogs
	// dogs
	// and
	// dogs
	// html-s5-or-rtf
	// applications
	//
	// kn3.v3
}
