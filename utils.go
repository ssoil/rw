package rw

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	fp "path/filepath"
	"regexp"
	"strings"
	"time"
)

func dump(thing interface{}) {
	fmt.Printf("%v\n", thing)
}

// Home returns the current user home directory.
func UserHome() string {
	u, _ := user.Current()
	return u.HomeDir
}

func exists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	}
	return false
}

func isDir(path string) bool {
	if i, err := os.Stat(path); err == nil {
		return i.IsDir()
	}
	return false
}

func env(name string) string {
	return os.Getenv(name)
}

// JSON return a readable JSON rendition of thing passed and adds ERROR if
// something went wrong while parsing. This is often used to fulfill the
// Stinger interface.
func JSON(thing interface{}) string {
	byt, err := json.MarshalIndent(thing, "", "  ")
	if err != nil {
		return fmt.Sprintf("{\"ERROR\": \"%v\"}", err)
	}
	return string(byt)
}

// JSONT is same as JSON just tiny (compressed) format.
func JSONT(thing interface{}) string {
	byt, err := json.Marshal(thing)
	if err != nil {
		return fmt.Sprintf("{\"ERROR\": \"%v\"}", err)
	}
	return string(byt)
}

// PathsWithREADME returns full paths to any path/*/README.md files.
func PathsWithREADME(path string) []string {
	readmes := []string{}
	inodes, err := ioutil.ReadDir(path)
	if err != nil {
		return readmes
	}
	for _, inode := range inodes {
		if inode.IsDir() {
			dir := fp.Join(path, inode.Name())
			readme := fp.Join(dir, "README.md")
			if _, err := os.Stat(readme); err == nil {
				readmes = append(readmes, readme)
			}
		}
	}
	return readmes
}

// CompileGlob takes a simple glob string and returns a compiled regular
// expression. This simplified glob syntax only allows the star (*) and
// question mark (?) for "any" and "one". For more complex matching use the
// regular expression syntax available with Find() using the tilde (~)
// prefixes.
func CompileGlob(glob string) *regexp.Regexp {
	// TODO make a single pass (less lazy) algorithm
	x := strings.ReplaceAll(glob, "?", ".")
	x = strings.ReplaceAll(glob, "\\.", "\\?")
	x = strings.ReplaceAll(glob, "*", ".*?")
	x = strings.ReplaceAll(glob, "\\.*?", "\\*")
	return regexp.MustCompile("(?i)" + x)
}

// ParseTime parses one of the following Go date format strings (in order of
// priority) and returns a time. These are used for time-based matching with
// Find().
//
//     mon
//     mon15
//     mon3p
//     jan
//     jan,2 jan2
//     jan,2,15 jan2,15
//     jan,2,1504 jan2,1504
//     jan,2006  jan2006
//     2006,jan 2006jan
//     2006
//     2006-01 200601
//     2006-01-02 20060102
//	 2006-01-02,15
//	 2006-01-02,1504
//	 2006-01-02 15:04:02
func ParseTime(s string) time.Time {
	/*
				   sss   - day of week, month
				   nnnn  - year
				   nnnn-nn  - year,month
				   nnnn-nn-nn  - year,month,day
				   nn    - day of month
			         nnnn - time
		               ,nn - hour
		               ,nnnn - hour, min
		               ,nnnnnn - hour, min, sec
	*/

	return time.Now()
}
