# README WorldPress, Personal Content Management System and Static Site Generator

> WIP with several commands still not usable.

Your personal publishing platform. Share what you know through the [README.world](https://readme.world) exchange using README WorldPress to search, read, write, and manage all your personal knowledge content, notes, articles, blog posts, resumes, quiz sheets, flashcards, presentation slides, and more.

## Getting Started

1. Find your favorite folder location, `Documents` perhaps.
1. Create a repo or directory called `README` in it.
1. Write a `README/README.md` file. This is your main README.
1. Create subdirectories inside `README` folder with dashed-lower names.
1. Write `README.md` files in each subdirectory.
1. Put any image and other files in the same subdirectory.
1. Build it all with `rw build`
1. Or, build and commit your repo with `rw save`.
1. Publish or auto-deploy with Netlify or other git commit triggered service.

That's just to get you started. After your first build the `~/Documents/README/.rw/` folder will be automatically created including the default `templates` that are used to turn your `README.md` files into `index.html` files.

Read more about usage in the [command help documentation](cmd/rw/).

## Primary Use Case

An individual (or small team) wants the freedom to publish content to any current or future media format, physical or digital, with the least amount of hassle using industry standard tools, without fear of future incompatibility, while maintaining full, versioned control of the content source to license and release as they see fit.

The primary content is for sharing and assisting learning in real time.

The main secondary content anticipated is to provide documentation with a single rhetorical voice, common set of well-researched opinions, and specific target audience (like books and unlike wikis).

Many other uses and content are expected based on the open nature of the tool and platform.

The tool must not require *any* configuration or initialization to use with defaults looking for a `READMEDIR` environment variable or a `README` directory in one of several possible default locations.

On first use the `.rw` directory structure must be created within the `README` directory containing all defaults where the user can alter them easily with any text editor.

All user configuration must be in YAML. 

All consumable meta data rendering must be in compressed JSON.

The following content formats must be including:

* Articles (similar to Medium)
* Chronological blog posts
* Notes and annotations
* Formal numbered documents
* Reference (cheat) sheets
* Spoilers (hidden) sections
* Resumes
* Video (linked or included)
* Audio (linked or included)

Composite formats, collections of the pointers to existing content, must also be supported to avoid redundancy and maintain modularity of the main source content:

* Aggregations or outlines
* Slide presentations
* Flashcards

Any editor will do and no GUI lock-in is required (as with existing content management systems).

Any source content must support TeX math notation and output to MathJax when rendered as HTML. (No current static site generator currently supports this.)

The most important output format should be as a progressive web app which can be used entirely offline and accessible over the Web including offline search capability (titles, subtitles, tags, categories, authors, publishing and revision dates, etc.)

The tool must provide rendered link maps with link integrity checking and reporting.

The tool must provide for HTML themes by format with minimal (but appealing) default themes automatically provided. The stylistic emphasis of the default theme must be on scientifically confirmed best readability and accessibility standards.

Where possible the meta data must follow internationally established standards for such.

The tool should provide summarized rendering of the following core meta data:

* Titles
* Categories
* Authors
* Contributors
* Tags
* Formats
* When published
* When last revised

The identifiers and titles of these summaries should be configurable and language agnostic with specific themes available.

The tool must primarily rely on `git` for all versioning and storage with preference for hosting on GitLab and GitHub but without a dependency on either service.

The tool does not need to provide any means of distribution or deployment other than through a `git` commit and push. 

The de facto HTML rendering must be done *before* storing and the rendered HTML stored with the core source files in the repository. This promotes local previewing *before* publishing. HTML rendering is, however, optional. All other rendering formats should be maintained outside of the source content repository.

The tool must provide real-time previewing capability for HTML rendered output similar to (if not dependent on) the industry standard Browsersync tool.

The tool must provide most all of the functionality (if not full dependency on) Pandoc, the academic standard for sustainable writing and publishing.

The tool must provide configurable push notification support.

The tool must have a default theme that allows the *reader* to fully customize the reading experience, fonts, dark mode, column width, font size, etc.

The tool must *not* provide any RSS functionality which would promote unnecessary web traffic to check for changes to the RSS feed. Videos and podcasts should instead be included as articles or notes with annotations that refer to the rich-media content elsewhere. In fact, the user is assumed to *never* place video or audio content within the README repo since it is fundamentally for text and images.

## Specification

Here are the points of the specification informally captured anticipating a formal document on it.

### One Level One Heading

* Each `README.md` file must contain only one level-one heading.
* The level-one heading must be the first heading in the `README.md` file.
* If the YAML meta data `title` is defined it must be considered the initial level-one heading.
* Any `README.md` must not contain both a `title` and an level-one heading.

## Pandoc Dependency

The `rw` tool fundamentally depends on Pandoc for syntax, format, and rendering but significantly simplifies the process. Use of Goldmark instead was considered (as Hugo uses) because at the core of knowledge capture and exchange is Math notation. *None* of the Markdown versions and extensions support any notion of Math.

Besides, Pandoc is the absolute leader in academic writing. Most modern textbooks are either written for Pandoc or are being rewritten to support it. It is far more important that the syntax of templates be Pandoc than anything else (including Hugo Go templates) because the Pandoc template system is the gold standard for producing documents and writing in *any* output format. This close Pandoc dependency, therefore, supports the widest and most powerful set of use cases possible.

### Pandoc Markdown

Pandoc Markdown is the official syntax of all `README.md` files (even though services like GitHub and GitLab don't support all of it). Pandoc supports advanced templating as well as rendering math notation allowing a far richer level of content. Pandoc Markdown is not focused on rendering only as HTML, but any of a dozen different target formats including JSON encapsulating its rich document abstract syntax tree (AST). This is why Pandoc has long since become a leading tool for academic publishing.

## YAML Front Matter and Pandoc Template Variables

Here are the supported properties that can appear in the YAML front matter (meta data) as well as others that are available to Pandoc template creators. They are roughly based on the [Dublin Core  Metadata Element Set](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), [Open Graph](https://ogp.me), [Twitter](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/abouts-cards), and (yes) [Facebook](https://developers.facebook.com/docs/sharing/webmasters). [We may not like Facebook but need to support it to allow content to reach Facebook users, who often need it more than others.] 

While there are a lot of formal standards for such things all of them are far to overcomplicated or unnecessary. Some are archaic. Some can be easily inferred from the content itself (such as **title**, the only first-level heading). The goal is simplicity, consistency, and fun. Many of these come from web writing terminology.

Note that none of these are required. In fact, YAML front matter is entirely optional. Critical properties will be inferred from the content itself.

Properties from the YAML front matter of the parent directory `README.md` file are also assumed to apply to every subdirectory if it is not set or cannot be inferred.

### **author**

The main creator of the content. This is always singular. If not provided it will be derived from the main `README.md` file.

### **contributors**

Any person or organization responsible for making contributions. Will be combined into the master `CONTRIBUTORS` file when built.

### **published**

Date and optional time of publication, even if internal. Often the same as creation time. (This fulfills the DCMI *date* definition.)

The following suffixed variations are also available:

-------------------- -------------------------------------------
 published            2019-11-28
 published-time       2019-11-28 11:11:44-0500
 published-unix       1574957449
-------------------- -------------------------------------------

Unfortunately only English `long` variations are supported. Merge requests are welcome for other common international long forms. The base Unix form can be combined with JavaScript to produce any other desired long form that is not included. These can also be used for dynamically updating age indications (ex: 1d 14h 9m ago).

### **revised**

Date of the last revision. Often referred to as the date and time content was significantly updated. (This fulfills the DCMI *revised* definition.) See [published] for list of suffixed forms also available.

### **modified**

Set automatically by looking at the last modified time on the `README.md` file (only).  See [published] for list of suffixed forms also available.

### **expired**

The moment after which the `README.md` is no longer valid. Build tools must observe and not include in build after the expiration time has passed. Such tools can also use this to clean up and remove all expired content. See [published].

### **title**

Title with only simplified inline Pandoc formatting. Will become the first (and should be only) first-level heading. If omitted will be inferred from an expected first-level heading. If neither is found will build with empty title and warn.

### **page-title**

Automatically set. Combination of [title] and [site-title-short] separated by a vertical bar and spaces (ex: `$title$ | $site-title-short$`). If the two are identical just the title will be used. If one or the other is empty then the vertical bar will be omitted.

### **subtitle**

Subtitle to the title. Will become a second-level heading immediately after the title first-level with the `{.subtitle}` class added. It the title was inferred so will the subtitle (ignoring any subtitle set in the YAML even if missing). This is to promote either inferring them both or setting explicitly in the YAML, not both. Will be ignored if no title is set. 

### **ident**

Should never be provided and will always be inferred from the name of the containing directory. To change it, change the name of the containing directory. Must comply with Pandoc auto-identifier limitations (lower-case, no spaces, etc.) (This fulfills the DCMI *identifier* definition.)

### **lang**

Automatically set. Implied from [locale]. (This fulfills the DCMI *language* definition.)

### **locale**

Much like [lang] but different. Will be set to system locale if omitted.

### **publisher**

An entity responsible for making the resource available. This is usually *not* the [author].

### **rights**

Information about rights held in and over the resource and any licensing sharing those rights. This is in addition to any special files included within the main `README` repo (i.e. LICENSE, COPYRIGHT, etc.)

### **summary**

Everything in 150 characters. Will be derived from first 150 characters of first paragraph if not set. (This fulfills the DCMI *description* definition.)

### **tags**

Multiple noun subjects separated by commas. Omit the hashtag. No spaces. No dashes or underscores. Think Twitter. (These are also combined into one string to fulfill the DCMI *subject* definition.)

### **category**

The spatial or temporal topic of the README, the spatial applicability of the README, or the jurisdiction         
under which the README is relevant. (This fulfills the DCMI *coverage* definition.)

### **format**

The format type of README file, for example one of the following:

* article 
* numbered
* spoilers
* sheet
* log
* video
* audio

And also these that refer to several individual README files:

* aggregate
* flashcards
* slides

Indicates the organization of the content in the `README.md` file. Specifies which template to use during building.

### **source**

The URL of the main source repository for the main README repo suitable for replication (ex: `git clone`). This property should only be in the main `README.md` file (and will be ignored everywhere else).

### **issues**

Information about how to resolve issues with the content (corrections, suggestions, etc.). This is usually a link.

### **site**

Where the rendered repo can be found on the web, including the preferred schema (ex: https://skilstak.io). This used to create the [canonical] property as well for individual subdirectories.

### **site-title**

Derived from the [title] of the main `README.md` file.

### **site-title-short**

Shorter version of the title used to combine with the titles of the individual `README.md` subdirectories for use in HTML `title` elements and such.

### **canonical**

Automatically set. Implied combination of the *site* and *ident* for the given `README.md`. Usually used in the base template for the similarly named `<link>` element. This allows replicated versions of the HTML rendered content to exist without confusing search engines.

### **weekday**

Automatically set. Number of the current weekday. Sunday is 1. Saturday is 7.

### **weekday-short**

Automatically set. Lowercase, English three-letter weekday: sun, mon, tue, wed, thu, fri, sat, sun.

### **weekday-long**

Automatically set. Lowercase, English weekday: sunday, monday, tuesday, wednesday, thursday, friday, saturday, sunday.

### **month-short**

Automatically set. Lowercase, English three-letter weekday: jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec.

### **month-long**

Automatically set. Lowercase, English three-letter weekday: january, february, march, april, may, june, july, august, september, october, november, december.

### **year**

Automatically set. Current year.

### **month**

Automatically set. Number of the current month. January is 1. December is 12.

### **day**

Automatically set. Day of the year.

### **number-sections**

Set to `true` to number the sections unless `{.unnumbered}`. (See Pandoc argument of same name.)

### **number-offset**

Set the offset number to begin with in this document. (See Pandoc argument of same name.)

## Simplified Markdown (Ezmark)

* `---` opening and closing YAML front matter, only at top.
* `----` for all separators, no other variations.
* `-----` or more for Pandoc tables.
* `\`\`\`` opening and closing code blocks.
* `~~~` or `~~~~` for code blocks containing Markdown.
* `*` for emphasis (italic).
* `**` for strong (bold).
* `***` for strong emphasis (bold italic).
* `'`, `"`, `--`, `---`, `...` always converted using smart-punctuation
* Blank lines must have zero length (no stray spaces).
* Only simple inline formatting in headings.
* Only ATX headings with hashtags.
* Single line paragraphs (better for modern editors).
* Only inline links on single line. 

## Design Considerations

Here are the design considerations and decisions made as they came up.

### The `rw` Name

The whole idea of maintaining a personal `README` repo is to make it as accessible as possible. The `rw` name is easiest to type, currently the most available, matches the `readme.world` domain, and invokes the very applicable notion of "read-write" and even the very old idea of "the readme web" (which was the original inspiration in 2014). The `rw` lends itself well to logo design as well.

### Use of `README` for Everything

The all capital `README` has become a de facto standard name for the core information needed about something in the software world. If follows, then that this also apply on a macro scale to individuals and small teams maintaining information about themselves. Indeed, it provides more colloquial and accurate term to describe such a collection of knowledge (v.s. "blog" for example):

* "It's in my README. Have a look when you can."
* "Let me add a README for that."
* "I'll add a tutorial README about that."

If follows that `README` is a master file name for all main content:

* `~/Documents/README/`
* `README.md`
* `README.json`

### Strong Search Support

Since the primary use case is an individual or small group creating small scale READMEs designed to be used modularly it makes sense to have strong search capability. The common search query shorthand language will be used consistently from terminal command line, query text boxes in graphic user interfaces, and in registries that span individual README repositories.

### Strong Composition Support

In addition to search capability, the ability to bring together modular content into larger compositions is also a priority. Books, guides, specifications, all are compositions.

### Title Markdown Inline Formatting 

All titles are allowed to contain inline [Simplified Pandoc Markdown] formatting which remains in the summary information `README.json` file and can actually be searched on as well.

### No RSS Support

RSS is a broken legacy approach to receiving notifications. It forces the redundant copying of information in other files into the central `rss.xml` file which is regularly requested from potentially millions of news aggregator applications. This technology was required before modern push notifications existed. 

Instead push notifications are fully supported and configurable.

### No Rich Media

Audio and video really have no place in a README repo keeping them light and easy to transfer. Instead links from articles, notes, and annotations to rich media sources should be used (YouTube, Soundcloud, Podbean, etc.)

### Use of Package Globals

Since the scope of the `README` project is to manage a single `README` repo on a single system, which is usually a personal workstation, it does not seem inappropriate to leverage package global variables for state such as the Jobs channel. If and when such becomes practical encapsulation of the Repo struct can be used without conflict. It seems best to keep as simple a package API as possible at the moment.

### Link Dependency Summary

This was a really tough decision. Ultimately including link dependencies in a `links.json` file separate from `README.json` seemed wisest since generally link dependencies are not something someone would search on. They do, however, provide strong insight into what data is most used in the README repo and *how* it is used. The obvious benefit of quickly identifying broken links is also well worth the resources required to maintain it.

### Dropped Overall Repo Stats from `README.json`

Initially it was thought that summarizing the number of tags and categories and such would be useful information to prepare and save in the `README.json` file. But this complicates the independent build process for any given `README` and is redundant. Just summaries and indexing can be best done by the front-end layer or by the additional, optional views that produce the following special HTML pages if enabled every time anything is built:

* `/dex/`
* `/categories/`
* `/authors/`
* `/contributors/`
* `/tags/`
* `/formats/`
* `/published/`
* `/revised/`

The exact names for each of these is configurable to support internationalization.

### Netlify-Centric

Netlify is clearly the best hosting service for publishing `README` repos to the Web.

### Just Bash Completion (No Zsh)

Zsh is currently the only other anticipated shell to be used with `rw` and it fully supports Bash completion when activated. Given the rock-solid nature and adoption of Bash completion globally it makes sense to simply support it instead of attempting two, however, the reserved subcommand `rw bashrc` allows for potential future addition of `rw zshrc` later if and when it makes sense.

### Redirects and Moves

Rather than leave a cluttered repo it was decided to detect the GitHub Pages and Netlify standard `_redirects` file and use that for renames and moves instead of putting a full `index.html` page in place with the meta redirect. This is easily done with an editor and renaming using the file system itself so there are no `rw` commands to do this.

### No Separate `build` Subdirectory

Keeping and committing the HTML with the source Markdown content together doubles the size of the repo but keeps the build artifact near the source much like building C code. This makes for easier debugging when adding HTML into the source itself. It also means that anyone forking or fetching the README repo won't have to build it and can preview locally without a web server.

In the rare case when the HTML must be removed it can easily be cleaned with `rw clean`.

This implies, of course, that building will be happening *before* any commit and deployment rather than having commits trigger automatic build and deployment. The justifications for the automated approach focus on multiple developers and the reduction of repo size neither of which apply here because more README repos will be authored by one or two primary authors who are following the same procedure of real-time authoring and local building.

It is also important to note that the rate at which changes and commits will be made to most README repos is significantly higher than with any software since commits are used as a near real-time publishing mechanism and most content will change quite frequently even throughout a single day. 

Indeed, it is better for individuals working on similar content to maintain their own individual README repos and interlink them through traditional hyperlinking. This focus on the individual, or small group of co-authors, is a radical departure from the founding principles behind the use of wikis and fosters content with a singular authors voice rather than attempting to cram multiple authors' voices --- often antagonistically --- into the same content. This is perhaps the single greatest failure of the wiki approach.

### Only `README.md` Files

No other type of file, even other Markdown files, are considered part of the main (and therefore indexed) content. For example, a `README.md` file might contain a local link to a `some.md` and the links from the `README.md` file will be returned from `Links()` without consideration for any links in `some.md`.

This encourages creation of granular content in flat, top-level subdirectories. It also allows other Markdown content to be included in subdirectories containing `README.md` files that are artifacts or examples related to the actual content. Strictly speaking the content of those files is not a part of the README repo at all (well, any more than an image would be).

### Left Out `Relation` from Dublin Core

Relations are implied by the inbound and outbound hyperlinks in the document itself --- especially those marked with the `{.see}` class.

### Left Out `Format` from Dublin Core

Everything is Pandoc Markdown and no plans exist to ever change that. Having a consistent format is a primary goal of this project.

### Eventual Migration Away from Pandoc

## TODO

Beyond completing this current version the following are planned eventually:

* Port [Browsersync](https://github.com/BrowserSync/browser-sync) to 100% Go package.
* Integrate [muffet](https://github.com/raviqqe/muffet) link checking.

