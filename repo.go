package rw

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"os/exec"
	fp "path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const API_VERSION = 1.0

// Repo encapsulates a README Repo containing a required README.json
// file, one or more optional, first level subdirectories also containing
// README.md files, and other files.
type Repo struct {

	// path to the repo directory on the local file system
	Path string `json:"-"`

	// URL of rendered web site with schema (ex: https://skilstak.io)
	Site string `json:",omitempty"`

	// long title for the entire README repo
	Title string `json:",omitempty"`

	// short title for combining in site bookmark, etc.
	TitleShort string `json:",omitempty"`

	// URL of the source suitable for replication (git, etc.)
	Source string `json:",omitempty"`

	// copyright, license, legal rights over content
	Rights string `json:",omitempty"`

	// URL of site where issues should be submitted
	Issues string `json:",omitempty"`

	// Info summarized from README.md data
	I map[string]*README `json:",omitempty"`
}

// String fulfills the Stringer interface.
func (r Repo) String() string {
	return JSON(r)
}

// Summarize scans the repo and updates both the README.json file and corresponding
// internal data. If there is any error the data and file remain unchanged.
func (r *Repo) Summarize() error {
	if r.Path == "" {
		return norepopath
	}
	// TODO
	return nil
}

// Save commits the state of the repo to the README.json file updating it.
func (r *Repo) Save() error {
	// TODO
	return nil
}

// Links returns map of all Links for the entire Repo keyed to the URLs
// of the link.
func (r *Repo) Links() Links {
	links := Links{}
	// TODO loop through the readme (I) data and build the To and From indexes
	return links
}

// Build asyncronously creates an index.html file for each README.md file,
// main and subdirectories. Only those README.md files that have last modified
// times later than that recorded in README.json will be included with the
// exception of those with type templates that have been updated matching
// .rr/templates/<type>.html A missing or updated template change triggers all
// files of that type to be remade. The index data for the READMEs that are made
// will be updated and Save() called to update the content of the README.json file.
func (r *Repo) Build() error {
	if r.Path == "" {
		return norepopath
	}
	// TODO call _repo.Build()
	//    for each README.md
	//          if it has been modified since cached
	//              load a new copy
	//              replace old copy in cache
	//              create a build job
	//              send the build job to the jobs channel

	// TODO write the cache to README.json
	// TODO get a list of all the subdirectories and the mod time on each
	// TODO filter the list to only those that have been changed
	// TODO create an async workgroup
	// TODO create a job for each update and send it to the workgroup
	// TODO wait for them to all finish
	return r.Save()
}

// MainFile returns the path to the main repo README.md file. This file
// contains defaults and other properties in its YAML front-matter data that
// apply to all README subdirectories as well. For example, the author set in
// the main README.md file will be assumed for every other README.md unless
// explicityly overridden.
func (r *Repo) MainFile() string {
	return fp.Join(r.Path, "README.md")
}

// InfoFile returns the path to the README.json info file. This file contains
// a summary of the README repo as well as key information from all the
// subdirectory README.md files. This file is usually enough for any front-end
// application to download and use to search without any need for an additional
// search engine. This means READMEs HTML templates can automatically include
// search capabilities such as for offline PWAs. Indeed, this file is key to
// the entire README initiative.
func (r *Repo) InfoFile() string {
	return fp.Join(r.Path, "README.json")
}

// LinksFile returns the path to the links.json file for this repo. This file
// contains an index of To and From links parsed from the main README.md and
// every subdirectory README.md file.
func (r *Repo) LinksFile() string {
	return fp.Join(r.Path, "links.json")
}

// Load loads the README.json file if it exists overriding any current settings.
func (r *Repo) Load() {
	byt, _ := ioutil.ReadFile(r.InfoFile())
	json.Unmarshal(byt, r)
}

// Preview builds the Repo and then waits for any changes and rebuilds. It also
// fires up a Browsersync previewer if found on the local system.
func (r *Repo) Preview() error {
	dump("would preview")
	return nil
}

// Edit looks for the this as a full README.Ident and if not finds all the
// matches for this in titles and identifiers and lists them prompting for user
// interaction to indicate which to edit. It then launches the preferred
// editor.
func (r *Repo) Edit(this string) error {
	dump("would open with editor")
	return nil
}

// Find returns all the README objects that match the query. The query string
// can be simple or complex. Multi-part queries are supported simply by
// separating with a space.
//
// Simple strings will be matched against the README's common properties in
// the following order:
//
//     Ident
//     Title
//     Subtitle,
//     Tags
//     Category
//     Format
//     Author
//     Contrib
//     Summary
//     Headings
//
// If any of the above contain each of the query string words a match will be
// returned. For example:
//
//     some (matches "Some Thing" title or "## Here's Some Heading")
//     blog rob (match Ident with "blog", Author of "Mr. Rob Muhlestein")
//
// Adding an hyphen/minus (-) in front of any part of the query string,
// plain or complex, will effectively add a "not" in front of it. For example:
//
//     -some (matches everything that does NOT have some in anything)
//
// Complex query strings are identified by a two character prefix. The
// first character indicates the field, the second (:,=,~,>,<) identifies the
// string as a complex prefix and provides instruction about how to apply the
// value of that part of the query string.
//
// The colon (:) identifies a simplified glob pattern (* -> any number of runes,
// ? -> any single rune, \ -> escape actual * or ?, never case sensitive):
//
//     i:<ident>
//     t:<title>
//     b:<subtitle>
//     g:<tag>
//     c:<category>
//     f:<format>
//     a:<author>
//     r:<contributor>
//     s:<summary>
//     h:<heading>
//     l:<language>
//
// Normally query strings match if it is found anywhere within the field
// matched (a substring). When an exact match is needed the equals sign (=) can
// be used:
//
//     i=readme-ssg
//     l=en
//
// The tilde (~) indicates a regular expression match (always case sensitive):
//
//     i~<ident>
//     t~<title>
//     b~<subtitle>
//     g~<tag>
//     s~<summary>
//     ...
//
// Any can be repeated for different match values. (See
// https://golang.org/pkg/regexp/).
//
// The following time-related fields also use equals (=) for exact matches:
//
//     p=<published>
//     v=<revised>
//     m=<modified>
//
// The value can be the stored Unix time (integer) or any of the following standard formats:
//
//	 2006-01-02 15:04:02
//	 2006-01-02 15:04:02Z
//	 2006-01-02 15:04:02-07
//	 2006-01-02 15:04:02-0700
//	 2006-01-02T15:04:02
//	 2006-01-02T15:04:02Z
//	 2006-01-02T15:04:02-07
//	 2006-01-02T15:04:02-0700
//
// However, the colon (:) operator can be used instead to get use the following shortcuts
// instead (in order of priority):
//
//     mon
//     mon15 mon,15 mon,3p mon3p
//     jan
//     jan,2 jan2
//     jan,2,15 jan2,15
//     jan,2,1504 jan2,1504
//     jan,2006  jan2006
//     2006,jan 2006jan
//     2006
//     2006-01 200601
//     2006-01-02 20060102
//	 2006-01-02,15
//	 2006-01-02,1504
//	 20060102,150402
//	 2006-01-02T15:04
//	 2006-01-02T15
//
// All of these are implied to be in the README timezone, which is usually
// the time zone of the local host locale.
//
// Only English month and weekday names are supported.
//
// Time related fields also support the less than (<) or greater than (>)
// characters to match only those that have times before or after the
// specified time.
//
// Each time related field value can have also have a plus (+) or minus (-)
// followed by one of the following time interval increments as well:
//
//     h - hours
//     d - days
//     m - months
//     y - years
//
// This allows ranges to be easily specified. If the field value does not have
// the time or date portion and simply begins with a minus or plus the time
// will be assumed to me offset from the current moment in time. For example,
//
//     p:-1h (everything published within the last hour)
//     r:-1y (everything revised within the last year)
//
// Note that the syntax of these query strings is deliberately minimal and easy
// to input from any device including mobile. The intention is that the same
// search API be implemented in JavaScript for use within offline progressive
// web apps enabling strong search capability without the need for
// a centralized search engine but can also be implemented in README registries
// such as readme.world.
//
func (r *Repo) Find(query string) []*README {

	// Do not attempt to break this out into a function that takes
	// a []*README slice because the defaults from the repo must be checked
	// when a README does not explicitly set a value such as Author for
	// example.

	results := []*README{}
	if query == "" {
		return results
	}
	qparts := strings.Split(query, " ")

readmes:
	for _, me := range r.I {

		// Check that every part of the query matches what it
		// should, when one doesn't force a continue to the
		// next readme.

		for _, q := range qparts {

			var not, found bool

			if q[0] == '-' {
				not = true
				q = q[1:]
			}

			// I know the following is verbose, but faster this way than
			// multiple function calls and here speed  matters. By the
			// way, https://github.com/robpike/filter

			pre := ""
			var op byte
			qv := q
			var rx *regexp.Regexp
			var tm time.Time

			// identify complex queries

			if len(q) > 1 {
				op = q[1]
				switch op {
				case ':', '~', '>', '<', '=':
					pre = q[0:2]
					qv = q[2:]
				}
				switch op {
				case ':':
					rx = CompileGlob(qv)
				case '~':
					rx = regexp.MustCompile(qv)
				case '>', '<':
					tm := ParseTime(qv)
					dump(tm)
				}
			}

			switch pre {

			case "": // simple queries (substring matches)

				if strings.Index(me.Ident, qv) >= 0 {
					if not {
						continue readmes
					}
					break
				}

				if strings.Index(me.Title, qv) >= 0 {
					if not {
						continue readmes
					}
					break
				}

				if strings.Index(me.Subtitle, qv) >= 0 {
					if not {
						continue readmes
					}
					break
				}

				found = false
				for _, g := range me.Tags {
					if strings.Index(g, qv) >= 0 {
						found = true
					}
				}
				if found {
					if not {
						continue readmes
					}
					break
				}

				if strings.Index(me.Category, qv) >= 0 {
					if not {
						continue readmes
					}
					break
				}

				if v, has := Formats[qv]; has {
					if me.Format == v {
						if not {
							continue readmes
						}
						break
					}
				}

				if strings.Index(me.Author, qv) >= 0 {
					if not {
						continue readmes
					}
					break
				}

				found = false
				for _, g := range me.Contrib {
					if strings.Index(g, qv) >= 0 {
						found = true
					}
				}
				if found {
					if not {
						continue readmes
					}
					break
				}

				if strings.Index(me.Summary, qv) >= 0 {
					if not {
						continue readmes
					}
					break
				}

				found = false
				for _, h := range me.Headings {
					if strings.Index(h.Ident, qv) >= 0 ||
						strings.Index(h.Title, qv) >= 0 {
						found = true
					}
				}
				if found {
					if not {
						continue readmes
					}
					break
				}

				if !not {
					continue readmes
				}

			case "i=": // -----------------------------------------------//
				match := me.Ident == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "t=":
				match := me.Title == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "b=":
				match := me.Subtitle == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "g=":
				found := false
				for _, tag := range me.Tags {
					if tag == qv {
						found = true
					}
				}
				if found && not || !found && !not {
					continue readmes
				}

			case "c=":
				match := me.Category == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "f=":
				s, err := strconv.Atoi(qv)
				if err != nil {
					if v, has := Formats[qv]; has {
						s = v
					} else {
						if !not {
							continue readmes
						}
					}
				}
				match := me.Format == s
				if match && not || !match && !not {
					continue readmes
				}

			case "a=":
				match := me.Author == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "r=":
				found := false
				for _, c := range me.Contrib {
					if c == qv {
						found = true
					}
				}
				if found && not || !found && !not {
					continue readmes
				}

			case "s=":
				match := me.Summary == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "h=":
				found := false
				for _, h := range me.Headings {
					if h.Ident == qv || h.Title == qv {
						found = true
					}
				}
				if found && not || !found && !not {
					continue readmes
				}

			case "l=":
				match := me.Language == qv
				if match && not || !match && !not {
					continue readmes
				}

			case "p=":
				// TODO match the exact integer
			case "v=":
				// TODO match the exact integer
			case "m=":
				// TODO match the exact integer

			case "i:", "i~": // -----------------------------------------------//
				match := rx.MatchString(me.Ident)
				if match && not || !match && !not {
					continue readmes
				}

			case "t:", "t~":
				match := rx.MatchString(me.Title)
				if match && not || !match && !not {
					continue readmes
				}

			case "b:", "b~":
				match := rx.MatchString(me.Subtitle)
				if match && not || !match && !not {
					continue readmes
				}

			case "g:", "g~":
				found := false
				for _, g := range me.Tags {
					if rx.MatchString(g) {
						found = true
					}
				}
				if found && not || !found && !not {
					continue readmes
				}

			case "c:", "c~":
				match := rx.MatchString(me.Category)
				if match && not || !match && !not {
					continue readmes
				}

			case "f:", "f~":
				s, err := strconv.Atoi(qv)
				if err != nil {
					if v, has := Formats[qv]; has {
						s = v
					}
				}
				if me.Format == s && not || me.Format != s && !not {
					continue readmes
				}

			case "a:", "a~":
				match := rx.MatchString(me.Author)
				if match && not || !match && !not {
					continue readmes
				}

			case "r:", "r~":
				found := false
				for _, g := range me.Contrib {
					if rx.MatchString(g) {
						found = true
					}
				}
				if found && not || !found && !not {
					continue readmes
				}

			case "s:", "s~":
				match := rx.MatchString(me.Summary)
				if match && not || !match && !not {
					continue readmes
				}

			case "h:", "h~":
				found := false
				for _, h := range me.Headings {
					if rx.MatchString(h.Ident) || rx.MatchString(h.Title) {
						found = true
					}
				}
				if found && not || !found && !not {
					continue readmes
				}

			case "l:", "l~":
				match := rx.MatchString(me.Language)
				if match && not || !match && !not {
					continue readmes
				}

			case "p:":
			case "p>": // -----------------------------------------------//
				//match := tm.Before(time.Now())
				//if match && not || !match && !not {
				//		continue readmes
				//	}
				dump(tm)
			case "p<":
				// TODO

			case "v:":
				// TODO
			case "v>":
				// TODO
			case "v<":
				// TODO

			case "m:":
				// TODO
			case "m>":
				// TODO
			case "m<":
				// TODO

			}
		}
		results = append(results, me)
	}
	return results
}

// Editor returns the full path to the preferred editor executable  that will
// be executed for any Edit calls. First the EDITOR environment variable is
// checked. Then checks for 'code' (Visual Studio Code). Then checks for 'vim'
// and finally 'vi'.  (Does not -- and will never -- check for nano, emacs, or
// sublime. Set EDITOR if that is your preference.)
func (r *Repo) Editor() string {
	// TODO great from .rr/config.yaml if exists
	editor := os.Getenv("EDITOR")
	editors := []string{
		"code",
		"vim",
		"vi",
		// don't you dare add anything else ;)
	}
	for _, e := range editors {
		this, _ := exec.LookPath(e)
		if this != "" {
			editor = this
			break
		}
	}
	return editor
}

// New creates a new README subdirectory and initializes it ready for editing.
func (r *Repo) New(ident string) error {
	// TODO
	return nil
}

// LoadRepo opens the specified Repo directory and returns it as Repo. Returns
// an empty Repo (with only Path set) if no README.json file is found within the
// directory. For new repos call Build() or GitSave() to generate the README.json file.
func LoadRepo(path string) *Repo {
	repo := new(Repo)
	repo.Path = path
	repo.Load()
	return repo
}
