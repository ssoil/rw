---
title: Rob Pike
subtitle: Brought You Go an Emojis
abstract: Rob Pike is one of three major Go creators.
category: person
format: article
keywords:
  - go
  - creator
  - utf-8
  - plan9
date: 2019-10-10
updated: 2019-10-25 18:43:10-05
author:
 - Rob Muhlestein <rwx@robs.io>
 - Another
---

Here is a paragraph. It is part of the first section which will have a `# Rob Pike` heading implied by the `title` YAML property.

```

# Should ignore

Blah

```

~~~

# How About

~~~

~~~~

And aanother

# An Another

~~~~

## Here is Another Title

Here are some code fences.

```

# But what about

``` 

## Another Second

Or if you have this exception.

~~~markdown

```markdown

# But what about

```

~~~ 

Or even this extreme exception.

~~~~markdown

~~~markdown

```markdown

# But what about 

```

~~~

~~~~

See that was tricky.

### Third level

Level three.

##### Fifth level

Level five.
##### Bad Fifth level

Level five.

###### Sixth level

Level six.

Another Level Two
-----------------

And something here.

##### Immediate Drop to Five

Woah.

### Should Crash?

Blah
