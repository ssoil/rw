---
source: https://gitlab.com/readme.world/rr/testdata/README
---

# Sample README Parent Directory

This is a KnowledgeNet README Repo. I has one main `README.md` file and any number optional first-level subdirectories all with heir own `README.md` files. You'll find a `rr.json` file here as well marking this as a README repo. The name of the directory is the preferred default name `README` which is generally recommended to live in your `$HOME/Documents` directory. It's almost always a Git repository but that is not strictly required. 

## Nothing Really Special

If you have done any software development in the last two decades you probably recognize this structure from that which has been used by software developers for years. That's really all this is, a specification of nothing particularly special that people already do all the time.

## Pandoc Markdown

The one thing the KnowledgeNet README Repos specification does call for is Pandoc Markdown instead of any other flavor. This is because Pandoc Markdown is far and away the industry standard for creating easy to write documentation today. From simple tables to complex match notation Pandoc Markdown does what no other Markdown has every successfully done.

The only potential annoyance from this is that services like GitLab and GitHub will not *automatically* show some parts of the `README.md` correctly, but since the whole point is to make it extremely easy to render as HTML (or any other document format) this is nothing but a minor annoyance (and frankly a problem with the short-sighted design of these services more than anything).

