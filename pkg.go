package rw

import (
	fp "path/filepath"
)

// Dir returns the directory which must contain a top-level README.md
// file and usually one or more subdirectories also with README.md files.
// Prioritizes based on the following:
//
// * $READMEDIR
// * $HOME/Sites/README
// * $HOME/Public/README
// * $HOME/Documents/README
// * $HOME/repos/README
// * $HOME/README
//
// Applications should depend on the user changing the READMEDIR to change
// the README directory workspace.
func Dir() string {
	home := UserHome()
	path := env("READMEDIR")
	if len(path) > 0 {
		return path
	}
	path = fp.Join(home, "Sites", "README")
	if isDir(path) {
		return path
	}
	path = fp.Join(home, "Public", "README")
	if isDir(path) {
		return path
	}
	path = fp.Join(home, "Documents", "README")
	if isDir(path) {
		return path
	}
	path = fp.Join(home, "repos", "README")
	if isDir(path) {
		return path
	}
	path = fp.Join(home, "README")
	if isDir(path) {
		return path
	}
	return ""
}
