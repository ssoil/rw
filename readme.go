package rw

import (
	"bufio"
	"fmt"
	"os"
	"path"
	fp "path/filepath"
	"strings"
	"time"
)

const (
	DATE = `2006-01-02`
	TIME = `2006-01-02 15:04:05-0700`
)

const (
	FMT_ARTICLE  int = iota + 1
	FMT_NUMBERED     // 1.1  1.2.3, etc.
	FMT_SPOILERS     // all but first section {.spoiler}
	FMT_SHEET        // YAML -> index.html, sheet, term
	FMT_LOG          // forward or reversed chronological
	FMT_NOTES        // notes, annotations, books, video, audio
	FMT_RESUME       // YAML -> resume.json, index.html

	// composites of base format types
	FMT_AGGREGATE int = iota + 100
	FMT_FLASHCARDS
	FMT_SLIDES
)

// Formats indicate how the content is organized in a standardized way that can
// be relied upon allowing different parsers and renderers to use it
// consistently. Some formats imply how the output is to be rendered as well.
// Most formats have their own template while others are always rendered the
// same way and rely entirely on style sheets for appearance.
var Formats = map[string]int{
	"article":    FMT_ARTICLE,
	"numbered":   FMT_NUMBERED,
	"spoilers":   FMT_SPOILERS,
	"sheet":      FMT_SHEET,
	"log":        FMT_LOG,
	"notes":      FMT_NOTES,
	"resume":     FMT_RESUME,
	"aggregate":  FMT_AGGREGATE,
	"flashcards": FMT_FLASHCARDS,
	"slides":     FMT_SLIDES,
}

// README encapsulates a single README.md file and any dependent files within
// its directory. Each README is composed of one or more Sections with the
// first Section always having the one and only level one Heading. Sections can
// be manipulated directly to add or remove sections. This is useful when
// combining or breaking apart READMEs.
//
// Many of the properties may be omitted and inferred from the same properties
// set for the entire Repo. (For a full description of README properties see
// https://spec.readme.world).
//
// The JSON tags are for the serialized string representation that is added ot
// the Repo.I (index).
type README struct {

	// no spaces (pandoc autoid), matches dir
	Ident string `json:"i,omitempty"`

	// standardized format identifier (ex: article)
	Format int `json:"f,omitempty"`

	// language code (default: <from repo>, <local system>)
	Language string `json:"l,omitempty"`

	// title (simplified markdown), becomes h1
	Title string `json:"t,omitempty"`

	// subtitle (simplified markdown), becomes first h2
	Subtitle string `json:"b,omitempty"`

	// 150 character tldr summary
	Summary string `json:"s,omitempty"`

	// creator-defined category
	Category string `json:"c,omitempty"`

	// list of Twitter-like hashtags (no hashtag)
	Tags []string `json:"g,omitempty"`

	// primary author and creator (default: <from repo>)
	Author string `json:"a,omitempty"`

	// contributors (default: <from repo>)
	Contrib []string `json:"r,omitempty"`

	// unix time of original publication date
	Published int64 `json:"p,omitempty"`

	// unix time of last date significantly revised from original
	Revised int64 `json:"v,omitempty"`

	// unix time of last detected modification for any reason
	Modified int64 `json:"m,omitempty"`

	// size in bytes of the file
	Size int64 `json:"-"`

	// local or repo path to icon (default: icon.png, /icon.png)
	Icon string `json:"-"`

	// local or repo path to thumb (default thumb.*, /thumb.*)
	Thumb string `json:"-"`

	// local or repo path to image (default: image.*, <first in README.md>)
	Image string `json:"-"`

	// summary, large (summary_large_image)
	TwitterCardType       string `json:"-"`
	TwitterCreatorAccount string `json:"-"` // userid
	TwitterSiteAccount    string `json:"-"` // userid
	OGType                string `json:"-"` // og:type (default: article)
	FBApp                 string `json:"-"` // fb:app_id

	// raw, untyped YAML frontmatter without modification
	Meta map[string]interface{} `json:"-"`

	// sequential headings
	Headings []Heading `json:"h,omitempty"`

	// sequential links
	Links []Link `json:"-"`

	repo *Repo
}

// String fulfills the Stringer interface.
func (r README) String() string {
	return JSONT(r)
}

// Heading is a portion of a README.md file that starts a Section a heading.
type Heading struct {
	Level int    // 1
	Ident string // rob-pike
	Title string // Rob Pike
}

// MarshalJSON converts Heading into a JSON array.
func (h Heading) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`[%v,"%v","%v"]`, h.Level, h.Ident, h.Title)), nil
}

// TODO UnmarshalJSON

func trimB(s string) string {
	l := len(s)
	if s[l-1] == '}' {
		n := strings.LastIndexAny(s[0:l-2], "{")
		if n >= 0 {
			return strings.TrimSpace(s[0:n])
		}
	}
	return s
}

func loadREADME(dir string) README {

	id := path.Base(dir)
	file := fp.Join(dir, "README.md")
	handle, _ := os.Open(file)
	defer handle.Close()

	hs := []Heading{}
	yml := ""
	readme := README{}

	// inferred variants
	title := ""
	subtitle := ""
	summary := ""

	info, _ := os.Stat(file)
	readme.Modified = info.ModTime().Unix()
	readme.Size = info.Size()

	// states
	inCode := false
	fence := ""
	fencel := 0
	wasBlank := true
	inYAML := false
	last := ""

	s := bufio.NewScanner(handle)
	for s.Scan() {
		line := s.Text()
		n := len(line)

		if n == 0 {
			wasBlank = true
			continue
		}

		switch {

		case !wasBlank:
			continue

		case inYAML:
			if n == 3 && line == "---" {
				inYAML = false
				continue
			}
			yml += line + "\n"
			continue
		case n == 3 && line == "---":
			inYAML = true
			continue

		case inCode:
			if n >= fencel && line[0:fencel] == fence {
				inCode = false
				fence = ""
				fencel = 0
			}
			continue
		case n >= 3 && line[0:3] == "```":
			inCode = true
			fence = "```"
			fencel = 3
			continue
		case n >= 4 && line[0:4] == "~~~~":
			inCode = true
			fence = "~~~~"
			fencel = 4
			continue
		case n >= 3 && line[0:3] == "~~~":
			inCode = true
			fence = "~~~"
			fencel = 3
			continue

		case n >= 3 && line[0:3] == "## ":
			md := line[3:]
			txt := trimB(md)
			hs = append(hs, Heading{2, AutoIdent(md), txt})
			if last[0:2] == "# " {
				subtitle = txt
			}
		case n >= 4 && line[0:4] == "### ":
			md := line[4:]
			hs = append(hs, Heading{3, AutoIdent(md), trimB(md)})
		case n >= 5 && line[0:5] == "#### ":
			md := line[5:]
			hs = append(hs, Heading{4, AutoIdent(md), trimB(md)})
		case n >= 6 && line[0:6] == "##### ":
			md := line[6:]
			hs = append(hs, Heading{5, AutoIdent(md), trimB(md)})
		case n >= 7 && line[0:7] == "###### ":
			md := line[7:]
			hs = append(hs, Heading{6, AutoIdent(md), trimB(md)})
		case n >= 2 && line[0:2] == "# ":
			md := line[2:]
			title = trimB(md)
			hs = append(hs, Heading{1, AutoIdent(md), title})

		case n > 0 && summary == "":
			i := strings.IndexRune(line, '.')
			if i > 0 && len([]rune(line[:i+1])) <= 150 {
				summary = line[:i+1]
			} else {
				runes := []rune(line)
				summary = string(runes[:149]) + "…"
			}

			// TODO parse inline links
		}
		last = line
		wasBlank = false
	}
	readme.Headings = hs
	readme.Ident = id

	// set core properties from meta data
	d := ParseYAML([]byte("---\n" + yml + "---\n"))
	readme.Meta = d

	if v, has := d["format"]; has {
		readme.Format = Formats[v.(string)]
	} else {
		readme.Format = 1
	}

	if v, has := d["language"]; has {
		readme.Language = v.(string)
	}

	if v, has := d["title"]; has {
		readme.Title = v.(string)
	} else {
		readme.Title = title
	}

	if v, has := d["subtitle"]; has {
		readme.Subtitle = v.(string)
	} else {
		readme.Subtitle = subtitle
	}

	if v, has := d["summary"]; has {
		readme.Summary = v.(string)
	} else {
		readme.Summary = summary
	}

	if v, has := d["category"]; has {
		readme.Category = v.(string)
	}

	if v, has := d["tags"]; has {
		for _, c := range v.([]interface{}) {
			readme.Tags = append(readme.Tags, c.(string))
		}
	}

	if v, has := d["author"]; has {
		readme.Author = v.(string)
	}

	if v, has := d["contrib"]; has {
		for _, c := range v.([]interface{}) {
			readme.Contrib = append(readme.Contrib, c.(string))
		}
	}

	if v, has := d["published"]; has {
		t, _ := time.Parse(DATE, v.(string))
		readme.Published = t.Unix()
	}
	if v, has := d["published-time"]; has {
		t, _ := time.Parse(TIME, v.(string))
		readme.Published = t.Unix()
	}
	if v, has := d["published-unix"]; has {
		readme.Published = v.(int64)
	}

	if v, has := d["revised"]; has {
		t, _ := time.Parse(DATE, v.(string))
		readme.Revised = t.Unix()
	}
	if v, has := d["revised-time"]; has {
		t, _ := time.Parse(TIME, v.(string))
		readme.Revised = t.Unix()
	}
	if v, has := d["revised-unix"]; has {
		readme.Revised = v.(int64)
	}

	return readme
}
