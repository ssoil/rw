package rw

import "fmt"

var norepopath = fmt.Errorf("Unspecified Repo Path.")
var cantfindrepo = fmt.Errorf("Cannot locate README dir in any default locations.")
