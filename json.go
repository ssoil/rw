package rw

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// ParseJSON reads bytes of JSON from and returns a data object.
func ParseJSON(byt []byte) map[string]interface{} {
	data := map[string]interface{}{}
	json.Unmarshal(byt, &data)
	return data
}

// LoadJSON loads a file containing JSON data specified by path into a data
// object. Returns empty data if unable to find or open the file.
func LoadJSON(path string) map[string]interface{} {
	byt, _ := ioutil.ReadFile(path)
	return ParseJSON(byt)
}

// ReadJSON reads all bytes available and passes them to LoadJSON.
func ReadJSON(r io.Reader) map[string]interface{} {
	byt, _ := ioutil.ReadAll(r)
	data := map[string]interface{}{}
	json.Unmarshal(byt, &data)
	return ParseJSON(byt)
}
