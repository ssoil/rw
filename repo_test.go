package rw

import "testing"

var repo Repo = Repo{
	Path:       "/some/path",
	Title:      "SkilStak Mentored Learning",
	TitleShort: "SKILSTAK",
	Site:       "https://skilstak.io",
	Source:     "http://gitlab.com/skilstak/io",
	Rights:     "(c) Rob Muhlestein, CC-BY-SA",
	Issues:     "https://gitlab.com/skilstak/io/issues",
	I: map[string]*README{
		"some-thing": {
			Ident:     "some-thing",
			Format:    2,
			Language:  "en",
			Title:     "Some Thing",
			Subtitle:  "A Subtitle",
			Summary:   "",
			Category:  "test",
			Tags:      []string{"tag1", "tag2", "foo"},
			Author:    "",
			Contrib:   []string{"another", "andanother"},
			Published: 20,
			Revised:   21,
			Modified:  24,
			Headings: []Heading{
				{1, "first-heading", "First Heading"},
				{2, "second-heading", "*Second* Heading Here"},
			},
		},
	},
}

func TestRepo(t *testing.T) {
	t.Log(repo)
}

func TestRepo_Editor(t *testing.T) {
	// TODO
	repo := LoadRepo("testdata/README")
	t.Log(repo.Editor())
}

func TestRepo_Find(t *testing.T) {
	results := repo.Find("h~^first")
	dump(results)
}
