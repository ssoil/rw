package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/skilstak/go/cmd"
)

func init() {
	_cmd := cmd.New("torunes")
	_cmd.Summary = `convert arguments into list of runes`
	_cmd.Usage = `<string> ...`
	_cmd.Description = `
            Combines the arguments into a single string and
            prints it as a list of runes.
      `
	_cmd.Method = func(args []string) error {
		text := ""

		if len(args) != 0 {
			text = strings.Join(args, " ")

		} else {
			byt, _ := ioutil.ReadAll(os.Stdin)
			text = string(byt)
		}

		quote := '`'
		if text[0] == '\'' {
			quote = '\''
			text = text[1:]
		}
		text = strings.TrimSpace(text)
		for _, r := range []rune(text) {
			fmt.Printf(`%v%v%v, `, string(quote), string(r), string(quote))
		}
		fmt.Println()
		return nil
	}
}
