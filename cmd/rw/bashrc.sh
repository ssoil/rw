export _rw=$(which rw);

setreadme () {
  if [[ -d "$HOME/Sites/README" ]]; then
    READMEDIR="$HOME/Sites/README"
  elif [[ -d "$HOME/Public/README" ]]; then
    READMEDIR="$HOME/Public/README"
  elif [[ -d "$HOME/Documents/README" ]]; then
    READMEDIR="$HOME/Documents/README"
  elif [[ -d "$HOME/repos/README" ]]; then
    READMEDIR="$HOME/repos/README"
  elif [[ -d "$HOME/README" ]]; then
    READMEDIR="$HOME/README"
  fi
  export READMEDIR
}

[[ -z "$READMEDIR" ]] && setreadme

rw () {
  case "$1" in 
    d) shift;
       mdir=$($_rw d $*);
       if [[ $? == 0 && $mdir != "" ]]; then
          echo "$mdir";
          cd "$mdir";
       fi
       ;;
    *) $_rw $* ;;
  esac;
};

export -f rw
complete -C rw rw
