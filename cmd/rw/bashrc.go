package main

import (
	"fmt"
	"gitlab.com/skilstak/go/cmd"
)

// semicolons just in case
// TODO make a generator pulling it from bashrc.sh
const BASHCODE = `
export _rw=$(which rw);

setreadme () {
  if [[ -d "$HOME/Sites/README" ]]; then
    READMEDIR="$HOME/Sites/README"
  elif [[ -d "$HOME/Public/README" ]]; then
    READMEDIR="$HOME/Public/README"
  elif [[ -d "$HOME/Documents/README" ]]; then
    READMEDIR="$HOME/Documents/README"
  elif [[ -d "$HOME/repos/README" ]]; then
    READMEDIR="$HOME/repos/README"
  elif [[ -d "$HOME/README" ]]; then
    READMEDIR="$HOME/README"
  fi
  export READMEDIR
}

[[ -z "$READMEDIR" ]] && setreadme

rw () {
  case "$1" in 
    d) shift;
       mdir=$($_rw d $*);
       if [[ $? == 0 && $mdir != "" ]]; then
          echo "$mdir";
          cd "$mdir";
       fi
       ;;
    *) $_rw $* ;;
  esac;
};

export -f rw
complete -C rw rw
`

func init() {
	_cmd := cmd.New("bashrc")
	_cmd.Summary = `print the recommended lines to eval from bashrc file`
	_cmd.Usage = ``
	_cmd.Description = `
            Prints the lines to source from a shell (or eval) providing
            the following shortcuts which require being loaded into the
            currently running shell to work properly:

            * An *rw* alias.
            * A *d* command that changes the current working directory.
            * Detect and set default *READMEDIR* env variable.

            All other subcommands are passed through directly.
      `
	_cmd.Method = func(args []string) error {
		fmt.Println(BASHCODE)
		return nil
	}
}
