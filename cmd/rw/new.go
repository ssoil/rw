package main

import (
	"strings"

	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rw"
)

func init() {
	_cmd := cmd.New("new")
	_cmd.Summary = `create a subdirectory with README.md and edit`
	_cmd.Usage = `<ident>`
	_cmd.Description = `
            Creates a new subdirectory within the main README directory
            and creates a new README.md file within it, then opens
            the file in the preferred editor for editing.

            For convenience the <ident> need not be quoted. In such cases
            the arguments will be joined with dashes 
            (ex: rw new some thing -> some-thing).
      `
	_cmd.Method = func(args []string) error {
		n := len(args)
		ident := ""

		switch n {
		case 0:
			return _cmd.UsageError()
		case 1:
			ident = args[0]
		default:
			ident = strings.Join(args, "-")
		}

		return rw.LoadRepo(rw.Dir()).New(ident)
	}
}
