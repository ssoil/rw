package main

import (
	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rw"
)

func init() {
	_cmd := cmd.New("preview")
	_cmd.Summary = `build automatically and preview locally`
	_cmd.Usage = ``
	_cmd.Description = `
            Builds any changes automatically and if browser-sync
            is installed will also run a local preview site (default
            port: 3000). This is useful for real-time, local,
            collaborative proofing and custom template creation.
      `
	_cmd.Method = func(args []string) error {
		n := len(args)
		if n != 0 {
			return _cmd.UsageError()
		}
		return rw.LoadRepo(rw.Dir()).Preview()
	}
}
