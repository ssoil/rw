package main

import (
	"fmt"

	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rw"
)

func init() {
	_cmd := cmd.New("dir")
	_cmd.Summary = `changes to specified subdirectory or main`
	_cmd.Usage = `[<ident>|<search>]`
	_cmd.Description = `
            Prints the full path to the specified README subdirectory.
            If bashrc support has been enabled will also change into
            the directory.

            If the argument does not match an existing README
            subdirectory searches for titles or identifiers that
            match and prompts.

            If no argument is found defaults to the content of the
            *READMEDIR* environment variable or one of the following
            defaults (in order of priority):

            1. $HOME/Sites/README  
            2. $HOME/Public/README  
            3. $HOME/Documents/README  
            4. $HOME/repos/README  
            5. $HOME/README  

            Note when bashrc support is enabled the READMEDIR
            environment variable is detected and set when the shell
            starts (if it is not already set explicitly).

            If none of the above match, returns without printing or doing
            anything.
      `
	_cmd.Method = func(args []string) error {
		fmt.Println(rw.Dir())
		return nil
	}
}
