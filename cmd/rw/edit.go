package main

import (
	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rw"
)

func init() {
	_cmd := cmd.New("edit")
	_cmd.Summary = `open a README.md in preferred editor`
	_cmd.Usage = `[<ident>|<search>]`
	_cmd.Description = `
            Opens a README.md from the specified subdirectory for
            editing in the preferred editor by executing the name
            of the preferred editor and passing it the full path
            to the README.md to be edited.

            If the argument does not match an existing README
            subdirectory name searches for titles or identifiers
            (including that of the main README.md) that
            match and prompts.

            If no argument is found defaults to the main README.md.
      `
	_cmd.Method = func(args []string) error {
		n := len(args)
		if n > 1 {
			return _cmd.UsageError()
		}
		this := ""
		if n > 0 {
			this = args[0]
		}
		return rw.LoadRepo(rw.Dir()).Edit(this)
	}
}
