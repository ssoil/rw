package main

import "gitlab.com/skilstak/go/cmd"

func init() {
	rw := cmd.New("rw",
		"new",     // create a new README and edit
		"edit",    // open a README with preferred editor
		"build",   // build anything changed since last build
		"preview", // build automatically and preview locally
		"dir",     // change into a README directory
		"mkident", // convert argument string into identifier
		"bashrc",  // print bash code suitable to eval
		"torunes", // print list of runs from combined args
	)
	rw.Summary = `README World Knowledge Exchange CLI Tool`
	rw.Version = `v0.0.1`
	rw.Copyright = `© Rob Muhlestein`
	rw.License = `Apache License 2.0`
	rw.Description = `
            Create, maintain, and publish static knowledge source content from a
            README directory (which is usually also a git repo).
      `
}
