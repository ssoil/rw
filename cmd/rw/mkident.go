package main

import (
	"fmt"
	"strings"

	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rw"
)

func init() {
	_cmd := cmd.New("mkident")
	_cmd.Summary = `convert arguments into Pandoc auto-identifier`
	_cmd.Usage = `<string>`
	_cmd.Description = `
            Combines the arguments into a single Pandoc
            auto-identifier string.
      `
	_cmd.Method = func(args []string) error {
		if len(args) == 0 {
			return _cmd.UsageError()
		}
		text := strings.Join(args, " ")
		fmt.Println(rw.AutoIdent(text))
		return nil
	}
}
