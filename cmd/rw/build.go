package main

import (
	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rw"
)

func init() {
	_cmd := cmd.New("build")
	_cmd.Summary = `build anything modified since last build`
	_cmd.Usage = ``
	_cmd.Description = `
            Builds anything that has changed since the last
            build and updates the index and meta data.
      `
	_cmd.Method = func(args []string) error {
		if len(args) != 0 {
			return _cmd.UsageError()
		}
		repo := rw.LoadRepo(rw.Dir())
		repo.Build()
		return nil
	}
}
