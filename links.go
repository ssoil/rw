package rw

import (
	"fmt"
)

const (
	LINK_FILE int = iota
	LINK_LOCAL
	LINK_EXTERNAL
	SEE_LOCAL
	SEE_EXTERNAL
)

// Link contains the information from any link contained within the text of a README.
type Link struct {
	Type int
	From string // Ident of the local README containing the link
	To   string // URL of the link, schema for external, / for repo, or local
	Text string // text associated with the link, empty for auto-links
	Ok   int    // status of link, -1 bork 0 unchecked, 1 good
}

// MarshalJSON converts Link into a JSON array.
func (l Link) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`[%v,"%v","%v","%v", "%v"]`,
		l.Type, l.From, l.To, l.Text, l.Ok)), nil
}

// TODO UnmarshalJSON

// Links contains indexes of the links in both directions.
type Links struct {
	To   map[string]*[]Link
	From map[string]*[]Link
}
