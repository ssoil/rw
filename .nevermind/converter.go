package rr

// Builder is an interface for converting an entire KnowledgeNet README Repo
// into another consumable format. Builder must not move or alter the source
// files in any way and can either produce additional files that are intermixed
// with the source files (like with the C make command) or create additional
// directories and files containing the output (like some build scripts that
// produce a 'dist' or 'build' directory). Keep in mind that anything created
// by the Builder must never be confused with original source files by any
// other potential Builder. The Builder is also responsible to clean up
// after itself when Clean() is called.
type Builder interface {

	// UID returns a unique ID (which must match the requirements for an
	// AutoIdent) which must be guaranteed to be universally unique for this
	// specific builder. This is suitable for creating dotted directory file
	// names to contain build artifacts, for example.
	UID() string

	// Build takes either a path to the top-level parent README directory or
	// a README struct. Builds must be transactional. Any error encountered
	// is returned on failure. Failed builds must leave the state of
	// everything exactly as it was, including any previous builds from the
	// same builder.
	Build(r interface{}) error

	// Clean takes either a path or README struct as well and will completely
	// remove any remnants of any previous call to Build. It is up to project
	// maintainers to ensure Builders never remove similarly named remnants from
	// other Builder implementations.
	Clean(r interface{}) error
}

// PandocHTMLBuilder is the default builder which calls the pandoc command
// to build each README.md into an index.html file within the same directory. After which
