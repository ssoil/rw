package main

import (
	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/ssoil/rr"
)

func init() {
	_cmd := cmd.New("save")
	_cmd.Summary = `builds, git commits, and pushes`
	_cmd.Usage = ``
	_cmd.Description = `
            Builds anything that has changed and then
            git commits and git pushes.
      `
	_cmd.Method = func(args []string) error {
		if len(args) != 0 {
			return _cmd.UsageError()
		}
		err := cmd.Call("build", args)
		if err != nil {
			return err
		}
		return rr.GitSave(rr.Dir())
	}
}
